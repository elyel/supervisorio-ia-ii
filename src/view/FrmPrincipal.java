/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import serial.Serial;

/**
 *
 * @author Elfab
 */
public class FrmPrincipal extends javax.swing.JFrame {
    private static final long serialVersionUID = -8914257590465162731L;
    
    Serial serialPort;
    String dadosRecebidosSerial = "";
    
    /**
     * Creates new form FrmPrincipal
     */
    public FrmPrincipal() {
        
        initComponents();
        serialPort = new Serial(this);
        this.setExtendedState(MAXIMIZED_BOTH);
        atualizarCombos();
    }

    public JTextArea getEtaLeituraSerial() {
        return etaLeituraSerial;
    }

    public void setEtaLeituraSerial(JTextArea etaLeituraSerial) {
        this.etaLeituraSerial = etaLeituraSerial;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpComunicacaoSeria = new javax.swing.JPanel();
        lbPortasDisponiveis = new javax.swing.JLabel();
        jcbPortasDisponiveis = new javax.swing.JComboBox();
        lbBaudRate = new javax.swing.JLabel();
        jcbBaudRate = new javax.swing.JComboBox();
        jbIniciarComunicacao = new javax.swing.JButton();
        jbFecharComunicacao = new javax.swing.JButton();
        lbDataBits = new javax.swing.JLabel();
        jcbDataBits = new javax.swing.JComboBox();
        lbStatus = new javax.swing.JLabel();
        jpGeral = new javax.swing.JPanel();
        jTabbedPane = new javax.swing.JTabbedPane();
        jlMonitorSerial = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        etaLeituraSerial = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jpSupervisao = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jlTensaoSensor1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jlNivelAgua = new javax.swing.JLabel();
        jlTensaoSensor2 = new javax.swing.JLabel();
        jlAnguloRegistro = new javax.swing.JLabel();
        jlSituacaoBomba = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jlStatusComporta = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jlVolumeAgua = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jlFalhaSensor0 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jlFalhaSensor1 = new javax.swing.JLabel();
        jlFalhaSensor2 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jpComunicacaoSeria.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Comunicação Serial"));

        lbPortasDisponiveis.setText("Portas disponíveis");

        lbBaudRate.setText("Baud Rate:");

        jcbBaudRate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1200 ", "2400 ", "4800 ", "9600 ", "19200 " }));
        jcbBaudRate.setSelectedIndex(3);

        jbIniciarComunicacao.setText("Iniciar Comunicação");
        jbIniciarComunicacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbIniciarComunicacaoActionPerformed(evt);
            }
        });

        jbFecharComunicacao.setText("Fechar Comunicação");
        jbFecharComunicacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbFecharComunicacaoActionPerformed(evt);
            }
        });

        lbDataBits.setText("Data bits:");

        jcbDataBits.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "7", "8" }));
        jcbDataBits.setSelectedIndex(1);

        lbStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbStatus.setText("STATUS");

        javax.swing.GroupLayout jpComunicacaoSeriaLayout = new javax.swing.GroupLayout(jpComunicacaoSeria);
        jpComunicacaoSeria.setLayout(jpComunicacaoSeriaLayout);
        jpComunicacaoSeriaLayout.setHorizontalGroup(
            jpComunicacaoSeriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpComunicacaoSeriaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpComunicacaoSeriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jbFecharComunicacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jcbPortasDisponiveis, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbIniciarComunicacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jcbBaudRate, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jcbDataBits, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbPortasDisponiveis)
                    .addComponent(lbBaudRate)
                    .addComponent(lbDataBits)
                    .addComponent(lbStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpComunicacaoSeriaLayout.setVerticalGroup(
            jpComunicacaoSeriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpComunicacaoSeriaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbPortasDisponiveis)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbPortasDisponiveis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbBaudRate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbBaudRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbDataBits)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbDataBits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbStatus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbIniciarComunicacao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbFecharComunicacao)
                .addContainerGap())
        );

        jTabbedPane.setMaximumSize(new java.awt.Dimension(950, 530));

        etaLeituraSerial.setColumns(20);
        etaLeituraSerial.setRows(5);
        jScrollPane1.setViewportView(etaLeituraSerial);

        jLabel2.setText("Monitor Serial");

        javax.swing.GroupLayout jlMonitorSerialLayout = new javax.swing.GroupLayout(jlMonitorSerial);
        jlMonitorSerial.setLayout(jlMonitorSerialLayout);
        jlMonitorSerialLayout.setHorizontalGroup(
            jlMonitorSerialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jlMonitorSerialLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jlMonitorSerialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
                    .addGroup(jlMonitorSerialLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jlMonitorSerialLayout.setVerticalGroup(
            jlMonitorSerialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jlMonitorSerialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane.addTab("Serial Monitor", jlMonitorSerial);

        jLabel3.setText("Tensão sensor 01: ");

        jLabel4.setText("Tensão sensor 02: ");

        jLabel5.setText("Ângulo Abertura Registro: ");

        jLabel6.setText("Seituação da Bomba:");

        jlTensaoSensor1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlTensaoSensor1.setForeground(new java.awt.Color(0, 102, 0));
        jlTensaoSensor1.setText("jLabel7");

        jLabel8.setText("Nível de Água:");

        jlNivelAgua.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlNivelAgua.setForeground(new java.awt.Color(0, 102, 0));
        jlNivelAgua.setText("jLabel7");

        jlTensaoSensor2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlTensaoSensor2.setForeground(new java.awt.Color(0, 102, 0));
        jlTensaoSensor2.setText("jLabel7");

        jlAnguloRegistro.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlAnguloRegistro.setForeground(new java.awt.Color(0, 102, 0));
        jlAnguloRegistro.setText("jLabel7");

        jlSituacaoBomba.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlSituacaoBomba.setForeground(new java.awt.Color(0, 102, 0));
        jlSituacaoBomba.setText("jLabel7");

        jLabel13.setText("Status da Comporta:");

        jlStatusComporta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlStatusComporta.setForeground(new java.awt.Color(0, 102, 0));
        jlStatusComporta.setText("jLabel7");

        jLabel15.setText("Volume de Água:");

        jlVolumeAgua.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlVolumeAgua.setText("jLabel7");

        jLabel17.setText("Falha Sensor 0:");

        jlFalhaSensor0.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlFalhaSensor0.setText("jLabel7");

        jLabel18.setText("Falha Sensor 1:");

        jlFalhaSensor1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlFalhaSensor1.setText("jLabel7");

        jlFalhaSensor2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlFalhaSensor2.setText("jLabel7");

        jLabel19.setText("Falha Sensor 2:");

        jButton1.setText("Abrir Comporta");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Fechar Comporta");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Ligar Bomba de Água");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Desligar Bomba de Água");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Parar o Sistema");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Iniciar o Sistema");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpSupervisaoLayout = new javax.swing.GroupLayout(jpSupervisao);
        jpSupervisao.setLayout(jpSupervisaoLayout);
        jpSupervisaoLayout.setHorizontalGroup(
            jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpSupervisaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpSupervisaoLayout.createSequentialGroup()
                        .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpSupervisaoLayout.createSequentialGroup()
                                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpSupervisaoLayout.createSequentialGroup()
                                        .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel15)
                                            .addComponent(jLabel17)
                                            .addComponent(jLabel18)
                                            .addComponent(jLabel19))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(jpSupervisaoLayout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(43, 43, 43)))
                                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlTensaoSensor2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jlTensaoSensor1, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                                        .addComponent(jlNivelAgua, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlStatusComporta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlVolumeAgua, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlFalhaSensor0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlFalhaSensor1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlFalhaSensor2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jlAnguloRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jlSituacaoBomba, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jpSupervisaoLayout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jpSupervisaoLayout.setVerticalGroup(
            jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpSupervisaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jlTensaoSensor1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jlTensaoSensor2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jlAnguloRegistro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jlSituacaoBomba))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jlNivelAgua))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jlStatusComporta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jlVolumeAgua))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jlFalhaSensor0))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jlFalhaSensor1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jlFalhaSensor2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE)
                .addGroup(jpSupervisaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton6, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jTabbedPane.addTab("Supervisão", jpSupervisao);

        javax.swing.GroupLayout jpGeralLayout = new javax.swing.GroupLayout(jpGeral);
        jpGeral.setLayout(jpGeralLayout);
        jpGeralLayout.setHorizontalGroup(
            jpGeralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpGeralLayout.setVerticalGroup(
            jpGeralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jpComunicacaoSeria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpGeral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpComunicacaoSeria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jpGeral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbIniciarComunicacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbIniciarComunicacaoActionPerformed
        if(jcbPortasDisponiveis.getSelectedItem() == null){
            lbStatus.setText("SELECIONE UMA PORTA");
        }
        String portaSerial = (String) jcbPortasDisponiveis.getSelectedItem();
        String sBaudRate = (String) jcbBaudRate.getSelectedItem();
        int baudRate = Integer.valueOf(sBaudRate.trim());
        serialPort.setSerialPort(portaSerial);
        serialPort.setBaudRate(baudRate);
        if(serialPort.openSerialPort().equals("SUCESSO")){
            lbStatus.setText("Porta Aberta");
        } else {
            lbStatus.setText("Erro ao abrir porta");
        }
    }//GEN-LAST:event_jbIniciarComunicacaoActionPerformed

    private void jbFecharComunicacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbFecharComunicacaoActionPerformed
        if(serialPort != null){
            serialPort.closeSerialPort();
            lbStatus.setText("PORTA LIBERADA");
        }
    }//GEN-LAST:event_jbFecharComunicacaoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<0>");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<1>");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<2>");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<3>");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<4>");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if(serialPort != null){
            serialPort.enviarDados("#<5>");
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea etaLeituraSerial;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JButton jbFecharComunicacao;
    private javax.swing.JButton jbIniciarComunicacao;
    private javax.swing.JComboBox jcbBaudRate;
    private javax.swing.JComboBox jcbDataBits;
    private javax.swing.JComboBox jcbPortasDisponiveis;
    private javax.swing.JLabel jlAnguloRegistro;
    private javax.swing.JLabel jlFalhaSensor0;
    private javax.swing.JLabel jlFalhaSensor1;
    private javax.swing.JLabel jlFalhaSensor2;
    private javax.swing.JPanel jlMonitorSerial;
    private javax.swing.JLabel jlNivelAgua;
    private javax.swing.JLabel jlSituacaoBomba;
    private javax.swing.JLabel jlStatusComporta;
    private javax.swing.JLabel jlTensaoSensor1;
    private javax.swing.JLabel jlTensaoSensor2;
    private javax.swing.JLabel jlVolumeAgua;
    private javax.swing.JPanel jpComunicacaoSeria;
    private javax.swing.JPanel jpGeral;
    private javax.swing.JPanel jpSupervisao;
    private javax.swing.JLabel lbBaudRate;
    private javax.swing.JLabel lbDataBits;
    private javax.swing.JLabel lbPortasDisponiveis;
    private javax.swing.JLabel lbStatus;
    // End of variables declaration//GEN-END:variables

    private void atualizarCombos() {
        //Atualizar portas seriais disponíveis.
        List<String> portasDisponiveis = serialPort.getAvailablePorts();
        for (String porta : portasDisponiveis) {
            jcbPortasDisponiveis.addItem(porta);
        }
    }

    
    public void atualizarSupervisorio(String informacao) {
        //Se a informação for nula retorna.
        if(informacao == null){
            return;
        }
        
        //Se a informação estiver em branco retorna
        if(informacao.isEmpty()){
            return;
        }
        
        // Se a informação não contem o caracter final do protocolo
        // Adiciona em uma string temporária  e retorna 
        // para que nas próximas leituras possa dar continuidade 
        // na informação recebida
        if(!informacao.contains(">")){
            this.dadosRecebidosSerial += informacao;
            return;
        } else {
            this.dadosRecebidosSerial += informacao;
        }
        
        // Cria uma string list para armazenar os comandos.
        List<String> comandos = new LinkedList<>();
        
        //Separa cada comando e adiciona na lista.
        while(this.dadosRecebidosSerial.contains(">")){
            comandos.add(this.dadosRecebidosSerial.substring(0, this.dadosRecebidosSerial.indexOf(">")+1));
            this.dadosRecebidosSerial = this.dadosRecebidosSerial.substring(this.dadosRecebidosSerial.indexOf(">")+1, this.dadosRecebidosSerial.length());
        }
        
        // Percorre a lista de comandos
        for (String comando : comandos) {
            System.out.println("CMD: " + comando);
            
            int idxPic = (comando.indexOf("#<") + 2);
            int idxTipoInformacao = idxPic + 1;
            int idxDados = idxTipoInformacao + 1;
            
            String pic = comando.substring(idxPic, idxPic+1);
            String tipoInformacao = comando.substring(idxTipoInformacao, idxTipoInformacao+1);
            String dados = comando.substring(idxDados, comando.indexOf(">"));

            System.out.println("PIC: " + pic);
            System.out.println("TIPO INFO: " + tipoInformacao);
            System.out.println("DADOS: " + dados);
            
            int iTipoInformacao = Integer.parseInt(tipoInformacao);
            
            switch(iTipoInformacao){
                case 0: // RANGE GERAÇÃO DE TENSÃO TURBINA 1
                    jlTensaoSensor1.setText(dados + " mV");
                    break;
                case 1: // RANGE GERAÇÃO DE TENSÃO TURBINA 2    
                    jlTensaoSensor2.setText(dados + "mV");
                    break;
                case 2: // AGULO DE ABERTURA DO REGISTRO
                    jlAnguloRegistro.setText(dados + "º");
                    break;
                case 3: // BOMBA AGUA
                    if(dados.endsWith("0")){
                        jlSituacaoBomba.setText(dados + " DESLIGADA");
                        jlSituacaoBomba.setForeground(new Color(153, 0, 0));
                    } else {
                        jlSituacaoBomba.setText(dados + " LIGADA");
                        jlSituacaoBomba.setForeground(new Color(0, 102, 0));
                    }
                    
                    break;
                case 4: // NIVEL DE AGUA
                    jlNivelAgua.setText(dados + " cm");
                    break;
                case 5: // STATUS COMPORTA
                    if(dados.endsWith("0")){
                        jlStatusComporta.setText(dados + " FECHADA");
                        jlStatusComporta.setForeground(new Color(153, 0, 0));
                    } else {
                        jlStatusComporta.setText(dados + " ABERTA");
                        jlStatusComporta.setForeground(new Color(0, 102, 0));
                    }
                    break;
                case 6: // VOLUME DE AGUA
                    jlVolumeAgua.setText(dados + "cm³");
                    break;
                case 7: // FALHA SENSOR
                    if(dados.startsWith("0") & dados.endsWith("0")){
                        jlFalhaSensor0.setText("FUNCIONANDO");
                        jlFalhaSensor0.setForeground(new Color(0, 102, 0));
                    } else if(dados.startsWith("0") & dados.endsWith("1")){
                        jlFalhaSensor1.setText("FUNCIONANDO");
                        jlFalhaSensor1.setForeground(new Color(0, 102, 0));
                    } else if(dados.startsWith("0") & dados.endsWith("2")){
                        jlFalhaSensor2.setText("FUNCIONANDO");
                        jlFalhaSensor2.setForeground(new Color(0, 102, 0));
                    } else if(dados.startsWith("1") & dados.endsWith("0")){
                        jlFalhaSensor0.setText("FALHA SENSOR 0");
                        jlFalhaSensor0.setForeground(new Color(153, 0, 0));
                    } else if(dados.startsWith("1") & dados.endsWith("1")){
                        jlFalhaSensor1.setText("FALHA SENSOR 1");
                        jlFalhaSensor1.setForeground(new Color(153, 0, 0));
                    } else if(dados.startsWith("1") & dados.endsWith("2")){
                        jlFalhaSensor2.setText("FALHA SENSOR 2");
                        jlFalhaSensor2.setForeground(new Color(153, 0, 0));
                    }
                    break;
                default:
                    break;
            }
        }
        
        // Limpa a lista de comandos para que no próximo loop não repita os 
        // mesmos comandos.
        comandos.clear();

    }
}
