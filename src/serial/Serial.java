/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package serial;

import gnu.io.SerialPortEvent;
import java.io.IOException;
import view.FrmPrincipal;

/**
 *
 * @author Elfab
 */
public class Serial extends JSerialPort{
    
    FrmPrincipal frmPrincipal;

    public Serial(FrmPrincipal frmPrincipal) {
        super();
        this.frmPrincipal = frmPrincipal;
    }

    @Override
    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            byte[] readBuffer = new byte[1000];
            try {
                int numBytes = 0;
                while (super.getInput().available() > 0) {
                    numBytes = super.getInput().read(readBuffer);
                }
                String result = new String(readBuffer);
                result = result.substring(0, numBytes);
                frmPrincipal.getEtaLeituraSerial().append(result);
                frmPrincipal.atualizarSupervisorio(result);

            } catch (IOException e) {
                System.err.println("SERIAL_EVENT>DATA_AVAILABLE: " + e.toString());
            }
        }
        
    }
    
    
}
