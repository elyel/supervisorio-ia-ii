/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chart;

import java.awt.BasicStroke;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.ThermometerPlot;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.ApplicationFrame;

/**
 *
 * @author Elfab
 */
public class TermometroChart extends ChartPanel{

    public TermometroChart(JFreeChart chart) {
        super(chart);
    }
    
    public static void main(final String[] args) {
        DefaultValueDataset dataset = new DefaultValueDataset(new Double(43.0));
        ThermometerPlot plot = new ThermometerPlot(dataset);
        plot.setThermometerStroke(new BasicStroke(2.0f));
        plot.setThermometerPaint(Color.lightGray);
        JFreeChart chart = new JFreeChart("Termometro", // chart title
                JFreeChart.DEFAULT_TITLE_FONT,
                plot, // plot
                false);          
        
       
        ChartPanel chartPanel = new ChartPanel(chart);
        JFrame frame = new JFrame();
        frame.add(chartPanel);
        frame.pack();
        frame.setVisible(true);

    }
}
